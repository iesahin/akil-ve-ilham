# Makefile
# 
# Converts Markdown to other formats (HTML, PDF, DOCX, RTF, ODT, EPUB) using Pandoc
# <http://johnmacfarlane.net/pandoc/>
#
# Run "make" (or "make all") to convert to all other formats
#
# Run "make clean" to delete converted files

# Convert all files in this directory that have a .md suffix

SOURCE_DOCS := $(sort $(wildcard manuscript/*.md))

EXPORTED_DOCS=\
 $(SOURCE_DOCS:.md=.html) \
 $(SOURCE_DOCS:.md=.epub) 

RM=/bin/rm

PANDOC=/usr/bin/pandoc

PANDOC_OPTIONS=--smart --standalone

PANDOC_HTML_OPTIONS=--to html5
PANDOC_PDF_OPTIONS=--latex-engine=xelatex
PANDOC_EPUB_OPTIONS=--to epub3


# Pattern-matching Rules

all: akil-ve-ilham.html akil-ve-ilham.epub

akil-ve-ilham.html : $(SOURCE_DOCS)
	$(PANDOC) $(PANDOC_OPTIONS) $(PANDOC_HTML_OPTIONS) -o $@ $(SOURCE_DOCS)

%.html : %.md
	$(PANDOC) $(PANDOC_OPTIONS) $(PANDOC_HTML_OPTIONS) -o $@ $<

akil-ve-ilham.epub : $(SOURCE_DOCS)
	$(PANDOC) $(PANDOC_OPTIONS) $(PANDOC_EPUB_OPTIONS) -o $@ $(SOURCE_DOCS) 

%.epub : %.md
	$(PANDOC) $(PANDOC_OPTIONS) $(PANDOC_EPUB_OPTIONS) -o $@ $<

# Targets and dependencies

.PHONY: all clean

all : $(EXPORTED_DOCS)

clean:
	$(RM) $(EXPORTED_DOCS) akil-ve-ilham.html akil-ve-ilham.epub
